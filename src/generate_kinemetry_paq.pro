;+
; NAME:
;	GENERATE_KINEMETRY_PAQ
;-
function generate_kinemetry_paq,pa,incl,radius,seeing,d0,ba_ratio=ba_ratio
	nrad 		= n_elements(radius)
	var		= seeing/d0
	p0 		= (0.25/var)+1.
	c 		= (1-exp(-p0*radius/d0))*(1.50+0.20*radius/d0)
	; previous adjustments of c(r)
	;c 		= (1-exp(-1.70*radius/seeing))*(1.29+0.25*radius/d0)
	;c 		= (1.0-exp(-radius/d0))*(exp(1)+((1.0+tanh(10*(seeing/d0-0.5)))/2.0)*(seeing/d0+2.2-exp(1)))
	ba_ratio 	= sqrt(((2.0*radius*cos(incl*!pi/180.))^2+(c*seeing)^2)/((2.0*radius)^2+(c*seeing)^2))
	paq 		= fltarr(2*nrad)
	for k=0L,2*nrad-2,2 do paq[k:k+1]=[pa-90,ba_ratio[k/2]]
	return,paq
end