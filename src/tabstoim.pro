;+
; This routine convert the tab format used in kinemetry routine
; to a image of a size n*n with n a multiple of 32
; You can also pass the dimension of the resulting image if you need so
;
; NAME:
;	TABSTOIM
;
; INPUTS:
;	XBIN:
;		An array containing the x postions of each bin
;	YBIN:
;		An array containing the y position of each bin
;	ZBIN:
;		An array containing the measurement associated to each bin
;
; OUTPUTS:
;	IM:
;		A 2D array containing the reconstructed image from the 3 inputs
;
; OPTIONAL INPUTS:
;	DIM:
;		User-defined dimension of the output image
;
; OPTIONAL OUTPUTS:
;	XCENTER:
;		The position of the X component of the center (middle of the map)
;	YCENTER:
;		The position of the Y component of the center (middle of the map)
;
;-
pro tabstoim,xbin,ybin,zbin,im,dim=dim,xcenter=xcenter,ycenter=ycenter
         
	if(n_params() ne 4) then begin
		doc_library,'tabstoim'
		return
	endif
        n1=n_elements(xbin)
        n2=n_elements(ybin)
        n3=n_elements(zbin)
        
        if(n1 ne n2 or n2 ne n3 or n1 ne n3) then begin
                message,"Tabs format is not valid."
                return
        endif else begin
                n=n1
        endelse
        
        if(keyword_set(dim)) then begin
                if(dim lt max(xbin)-min(xbin) or dim lt max(ybin)-min(ybin)) then begin
                        print,"dim keyword should be greater than the passed value"
                        return
                endif
        endif
        
        if(~keyword_set(dim)) then begin
                dim = 16L
                while(max(xbin) ge (dim-1)/2.D or max(ybin) ge (dim-1)/2.D $
                        or abs(min(xbin)) ge (dim-1)/2.D or abs(min(ybin)) ge (dim-1)/2.D) do begin 
                        dim+=2L
                endwhile
        endif
        im=fltarr(dim,dim)
        im[*,*]=!values.f_nan
        for i=0L,n-1L do begin
                x=xbin[i]+(dim-1L)/2.D
                y=ybin[i]+(dim-1L)/2.D
                im[floor(x),floor(y)] = zbin[i]
        endfor
        
        xcenter=(dim-1)/2.D
        ycenter=(dim-1)/2.D
end