pro plot_kinemetry_output,xbin,ybin,velbin,velcirc,velkin,errbin,xshift,yshift,npix=npix
        if(~keyword_set(npix)) then npix=256
        ; We transform arrays to images
        tabstoim,xbin,ybin,velbin,velbin_im,xcenter=xc,ycenter=yc
        tabstoim,xbin,ybin,velkin,velkin_im
        tabstoim,xbin,ybin,velcirc,velcirc_im
        ; We define the size of the resulting image
        sizex=(size(velbin_im))[1]
        sizey=(size(velbin_im))[2]
        xc-=xshift
        yc-=yshift
        ; We localize which pixels were triangulated 
        not_triangulated=where(velkin eq 23456789,complement=triangulated)
        ; If not triangulated, the value is set to NaN
        velkin_im[not_triangulated]=!values.f_nan
        velcirc_im[not_triangulated]=!values.f_nan
        ; We localize the pixels with NaN values in the previous image
        idx=where(finite(velbin_im[*,*]),complement=idx_nan)
        ; We resize the arrays, excluding non-triangulated bins
        resized_xbin=xbin(triangulated)
        resized_ybin=ybin(triangulated)
        resized_velbin=velbin(triangulated)
        resized_velkin=velkin(triangulated)
        resized_velcirc=velcirc(triangulated)
        resized_errbin=errbin(triangulated)
        ; We convert resized arrays to new images
        tabstoim,resized_xbin,resized_ybin,resized_velbin,velbin_im_r,xcenter=xc_r,ycenter=yc_r
        tabstoim,resized_xbin,resized_ybin,resized_velkin,velkin_im_r
        tabstoim,resized_xbin,resized_ybin,resized_velcirc,velcirc_im_r
        ; We define the size of the resulting images
        sizex_r=(size(velbin_im_r))[1]
        sizey_r=(size(velbin_im_r))[2]
        xc_r-=xshift
        yc_r-=yshift
        ; We set the color scale of the plot
        maxi_vel = max(velbin_im,/nan)
        mini_vel = min(velbin_im,/nan)
        ; We rescale each image
        toplot_velbin_im=rescale_im(velbin_im,mini_vel,maxi_vel)
        toplot_velbin_im_r=rescale_im(velbin_im_r,mini_vel,maxi_vel)                        
        ;Rebin the image in order to have a proper resolution
        toplot_velbin_im=congrid(toplot_velbin_im,npix,npix)
        toplot_velbin_im_r=congrid(toplot_velbin_im_r,npix,npix)
        toplot_velcirc_im=rescale_im(velcirc_im,mini_vel,maxi_vel)
        toplot_velcirc_im_r=rescale_im(velcirc_im_r,mini_vel,maxi_vel)
        ;Rebin the image in order to have a proper resolution
        toplot_velcirc_im=congrid(toplot_velcirc_im,npix,npix)
        toplot_velcirc_im_r=congrid(toplot_velcirc_im_r,npix,npix)
        toplot_velkin_im=rescale_im(velkin_im,mini_vel,maxi_vel)
        toplot_velkin_im_r=rescale_im(velkin_im_r,mini_vel,maxi_vel)
        ;Rebin the image in order to have a proper resolution
        toplot_velkin_im=congrid(toplot_velkin_im,npix,npix)
        toplot_velkin_im_r=congrid(toplot_velkin_im_r,npix,npix)
        ; We compute the residuals arrays of the B1 term of the kinemetry
        circ_residuals = sqrt((velcirc_im_r-velbin_im_r)^2)
        ; We convert resized arrays to new images
        toplot_circ_residuals = rescale_im(circ_residuals,min(circ_residuals,/nan),max(circ_residuals,/nan))
        toplot_circ_residuals=congrid(toplot_circ_residuals,npix,npix)
        ; We compute the residuals arrays of all the terms of the kinemetry
        kin_residuals = sqrt((velkin_im_r-velbin_im_r)^2)
        toplot_kin_residuals = rescale_im(kin_residuals,min(circ_residuals,/nan),max(circ_residuals,/nan))
        toplot_kin_residuals=congrid(toplot_kin_residuals,npix,npix)
        ; Because the images were rebinned, we redefine their size
        sizex = npix
        sizey = npix
        sizex_r = npix
        sizey_r = npix
        ; We set the center coordinates of the kinemetry run
        xc*=float(sizex)/(size(velbin_im))[1]
        yc*=float(sizey)/(size(velbin_im))[2]
        xc_r*=float(sizex_r)/(size(velbin_im_r))[1]
        yc_r*=float(sizey_r)/(size(velbin_im_r))[2]
        ; We set the window device for plotting
        wset,0
        if(!D.WINDOW eq 0 and !D.X_SIZE ne 2*sizex and !D.Y_SIZE ne 2*sizey) then  begin
                window,0,xs=2*sizex,ys=2*sizey,title='Kinemetry Fit' 
        endif
        ; Plotting images
        loadct,40,/silent
        tv,toplot_velbin_im,0.0,0.5,/normal
        tv,toplot_velcirc_im_r,0.5,0.5,/normal
        tv,toplot_velbin_im_r,0.0,0.0,/normal
        tv,toplot_circ_residuals,0.5,0.0,/normal
        ; Plotting contours
        loadct,0,/silent     
        ;contour,toplot_velbin_im,nlevels=3,position=[0.0,0.5,0.5,1.0],/normal,/noerase,xstyle=1,ystyle=1,ytickformat='(A1)',xtickformat='(A1)'
        ; Plotting a cross to visualize the center
        plot,[xc],[yc],psym=7,thick=3.,symsize=2.,color=0,position=[0.0,0.5,0.5,1.0],/normal,/noerase,xstyle=1,ystyle=1,$
                ytickformat='(A1)',xtickformat='(A1)',xrange=[0,sizex],yrange=[0,sizey]
        oplot,[xc],[yc],psym=7,thick=2.,symsize=2. 
        ;contour,toplot_velcirc_im_r,nlevels=3,position=[0.5,0.5,1.0,1.0],/normal,/noerase,xstyle=1,ystyle=1,ytickformat='(A1)',xtickformat='(A1)'
        ;contour,toplot_velbin_im_r,nlevels=3,position=[0.0,0.0,0.5,0.5],/normal,/noerase,xstyle=1,ystyle=1,ytickformat='(A1)',xtickformat='(A1)'
        plot,[xc_r],[yc_r],psym=7,thick=3.,symsize=2.,color=0,position=[0.0,0.0,0.5,0.5],/normal,/noerase,xstyle=1,ystyle=1,$
                ytickformat='(A1)',xtickformat='(A1)',xrange=[0,sizex_r],yrange=[0,sizey_r]
        oplot,[xc_r],[yc_r],psym=7,thick=2.,symsize=2.
        xyouts,0.7,0.4,"max res = "+string(strtrim(max(circ_residuals,/nan),1))+" km/s",/normal
        
        
        ;contour,toplot_circ_residuals,nlevels=3,position=[0.5,0.0,1.,0.5],/normal,/noerase,xstyle=1,ystyle=1,ytickformat='(A1)',xtickformat='(A1)'
end