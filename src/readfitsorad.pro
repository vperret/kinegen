function readfitsorad,file,header,_extra=extra
	splitted = (strsplit(file,'.',/extract))
	extension = splitted[n_elements(splitted)-1]
	if(extension eq 'fits') then begin
                im=readfits(file,header,_extra=extra)
        endif
        if(extension eq 'ad2') then begin
                im=readad2(file,trailer)
                wh = where(im eq -3.10000e+38)
                if(wh[0] ne -1) then im[wh] = !values.f_nan
                res = ad2TrailerToFitsHeader(trailer,header)
        endif
        if(extension eq 'ad3') then begin
                im=readad3(file,trailer)
                im[where(im eq -3.10000e+38)] = !values.f_nan
                res = ad3TrailerToFitsHeader(trailer,header)
        endif
        if(extension ne 'ad2' and extension ne 'ad3' and extension ne 'fits') then begin
                 print,'Bad file format'
                 print,'Cannot convert this file to tabs'
                 return,-1
        endif
        close,/all
        return,im
end