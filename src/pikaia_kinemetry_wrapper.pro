;+
;
;-
function pikaia_kinemetry_wrapper,n,p,parinfo=parinfo,xbin=xbin,ybin=ybin,errbin=errbin,velbin=velbin,$
        plotall=plotall,verbose=verbose,velkin=velkin,seeing=seeing,rwd_index1=rwd_index1,rwd_index2=rwd_index2,cover=cover,$
        radius=radius,cvalue=cvalue,cfvel=cfvel,er_cfvel=er_cfvel,min_pix=min_pix,$
        rc_max_vdep=rc_max_vdep,rc_min_slope=rc_min_slope,rc_max_delta=rc_max_delta,rc_max_vr0=rc_max_vr0,$
        max_ell_sampl=max_ell_sampl
                
        if((size(p))[0] gt 1) then np = (size(p))[2]
        if((size(p))[0] eq 1) then np = 1
        fitness         = dblarr(np)
        last_incl       = dblarr(np)
        ; We transform arrays into image in order to evaluate if there is enough pixels
        ; to run kinemetry
        tabstoim,xbin,ybin,velbin,velbin_im,xcenter=xc,ycenter=yc
        sizex=(size(velbin_im))[1]
        sizey=(size(velbin_im))[2]        

	params=fltarr(4)
        not_fixed = where(parinfo.fixed eq 0,ct,complement=fixed)
        if(fixed[0] ne -1) then params[fixed] = parinfo[fixed].VALUE
        
        for i=0L,np-1 do begin
                ; We define the parameters of the kinemetric fit
                if((size(p))[0] gt 1) then begin
                	params[not_fixed] = p[*,i]*(parinfo[not_fixed].limits[1]-parinfo[not_fixed].limits[0])+parinfo[not_fixed].limits[0]
                endif else begin
                        params[not_fixed] = p[*]*(parinfo[not_fixed].limits[1]-parinfo[not_fixed].limits[0])+parinfo[not_fixed].limits[0]
                endelse
		pa	= params[0]
		incl	= params[1]
		x_shift	= params[2]
		y_shift	= params[3]

                ; Estimating the center for kinemetry analysis
                ; in pixels coordinates
		nrad = n_elements(radius)
		incl_psf=sqrt(((2.0*radius*cos(incl*!pi/180.))^2+(cvalue*seeing)^2)/((2.0*radius)^2+(cvalue*seeing)^2))
                paq=fltarr(2*nrad)
                ; We store in an array the values of PA and ellipse flattening
                for k=0L,2*nrad-2,2 do paq[k:k+1]=[pa-90,incl_psf[k/2]]
                
                ; We test if its possible to draw the first ellipse within the given parameters
                ; If the first ellipse does not satisfy the coverage criterion, the returned fitness is zero.
                
		if(test_kinemetry(paq,radius,xbin+x_shift,ybin+y_shift,velbin,cover=cover) ne 1) then goto,fail                
                
                if(keyword_set(verbose)) then begin
                        print,"Running Kinemetry with following parameters: "
                        print," PA="+strtrim(string(pa),1)+" i="+strtrim(string(incl),1)$
                                +" center=["+strtrim(string(x_shift),1)+","+strtrim(string(y_shift),1)+"]"
                endif
                xbin2 = xbin+x_shift
                ybin2 = ybin+y_shift
                velbin2  = velbin
                undefine,velkin
                undefine,velcirc
                ;-----------------------------
                ; Launching kinemetry analysis
                ; We are using a slighltly modified version of the original code
                ; wrote by Davor Krajnovic (Krajnovic et al 2006)
                kinemetry,xbin2,ybin2,velbin2,rad,rpa,rq,cfvel,/all,$
                        ntrm=10,error=errbin,paq=paq,er_cf=er_cfvel,er_pa=paErr,er_q=er_q,$
                        velkin=velkin,velcirc=velcirc,xell=xell,yell=yell,cover=cover,nrad=nrad,$
                        radius=radius,/bmodel,scale=1.00,max_ell_sampl=max_ell_sampl
                ;-----------------------------
                ; Look for non-triangulated pixels
                if(velkin[0] eq -1) then goto,fail
                not_triangulated = where(velkin eq 23456789,complement=triangulated)
                if(triangulated[0] eq -1) then goto,fail
                if(n_elements(triangulated) le 4) then goto,fail
                if(n_elements(triangulated) le min_pix*n_elements(velbin)) then goto,fail
                ; Check that the recovered de-projected rotation velocity is lower than rc_max_vdep
                ; This prevents fits of edge-on disk with vero low-inclination
                if(max(abs(cfvel[*,2]))/sin(incl*!pi/180.) ge rc_max_vdep) then goto,fail
                ; Check that the slope of the RC is greater than rc_min_slope
                lin_coeff = linfit(rad,abs(cfvel[*,2]))
                if(lin_coeff[1] lt rc_min_slope) then goto,fail
                ; Check that there is no strong oscillations in the RC
                for j=0L,n_elements(rad)-2 do if(abs(cfvel[j,2]-cfvel[j+1,2]) gt rc_max_delta*abs(cfvel[j,2])) then goto,fail
                ; Check that the RC starts with a velocity lower than rc_max_vr0
                if(abs(cfvel[0,2]) gt rc_max_vr0) then goto,fail
                nbin_velcirc	= n_elements(triangulated)
                nbin_velbin 	= n_elements(velbin)
                ; Resizing the arrays in order to have exclusively well-triangulated values
                rszd_velbin 	= velbin[triangulated]
                rszd_velcirc 	= velcirc[triangulated]
                rszd_errbin 	= errbin[triangulated]
                ; Plotting each kinemetry run
                if(keyword_set(plotall)) then plot_kinemetry_output,xbin,ybin,velbin,velcirc,velkin,errbin,x_shift,y_shift
                ; Here we set a merit factor because each run can have a different number of pixels
                ; If we don't assign a merit factor to results where there is more pixels than average,
                ; the fit converge to a solution with a very low number of pixels
                ; A power-law with a significant power has to be defined to avoid local minimum with few pixels in chi2
                ; The more we get closer to the original number of pixels in the input image,
                ; the more the fitness is rewarded
                rbin 		= sqrt(xbin2^2+ybin2^2)
                junk 		= min(rbin,closest)
        	flux_center	= errbin(closest)
                merit_factor 	= (1.0/flux_center)^rwd_index2*(double(nbin_velcirc)/double(nbin_velbin))^rwd_index1
                chi2 		= double(total(((rszd_velbin-rszd_velcirc)^2/rszd_errbin^2)))/$
                		(double(nbin_velcirc)-double(n_elements(where(parinfo.fixed eq 0))-1))
                if(chi2 eq 0) then begin
                	print,'Watch out! Unrealistic chi2 -> the fit will not converge correctly'
		endif
                fitness[i] 	= merit_factor/chi2
                if(fitness[i] lt 0.D) then begin
                	print,'Watch out! Unrealistic fitness -> the fit will not converge correctly'

                endif
                ;Store the inclination of the last ellipse
                last_incl[i]=acos(incl_psf[n_elements(rad)-1])*180./!pi
                ; Print parameters on standard output
                continue
                ; Cases where the center is not enough well-positionned to perform kinemetry analysis
                fail:
                fitness[i]=0.
        endfor
        return,reform(fitness)
end