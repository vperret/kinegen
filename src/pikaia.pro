pro def_common_blocks
	common share1,iseed
        common share2,iv,iy,idum2
        common share3,fname,xtra,pinfo
        common share4,threading,bridges,n_threads,start_time
        common synchro,wait_time,irep,nexec,nsync
        common datstore,bestft,pmutpv,wrep_ind
end 

pro init_threads
        common share3
        common share4
        common synchro
        n_threads=!cpu.tpool_nthreads
        bridges = ptrarr(n_threads)
        wait_time = 1.0
        for i=0L,n_threads-1 do begin
                bridges[i] = ptr_new(obj_new('IDL_IDLBridge',output=''))
                (*bridges[i])->execute,'@'+pref_get("IDL_STARTUP")
                (*bridges[i])->setvar,"fname",fname
                if(size(xtra,/type) eq 8) then struct_pass,xtra,(*bridges[i])
                struct_pass,pinfo,(*bridges[i])
        endfor
end

pro kill_threads
        common share4
        for i=0L,n_threads-1 do begin
                (*bridges[i])->abort
                WHILE (*bridges[i]->Status() NE 0) DO WAIT, 0.5
                obj_destroy,(*bridges[i])
        endfor
end

function bridge_function_wrapper,funcname,n,oldph,_extra=extra
        common share3
        common share4
        common synchro
        if(~threading) then goto,nothreading
        npop = (size(oldph))[2]
        if(npop lt n_threads) then n_threads = npop
        st = indgen(n_threads)*floor(npop/n_threads)
        nd = (indgen(n_threads)+1)*floor(npop/n_threads)
        nd[*]-=1
        res = (size(oldph))[2] mod n_threads
        ind = 1
        while(res gt 0) do begin
                if(ind gt n) then ind =1
                for i=ind,n-1 do st[i]+=1
                for i=ind-1,n-1 do nd[i]+=1
                ind+=1
                res-=1
                
        endwhile
        fitns = dblarr(npop) 
        for i=0L,n_threads-1 do begin
                (*bridges[i])->setvar,"n",n 
                (*bridges[i])->setvar,"oldph",oldph[0:n-1,st[i]:nd[i]]
                (*bridges[i])->execute,"fitns=call_function(fname,n,oldph,parinfo=pinfo,_extra=xtra)",/nowait
        endfor
        status=intarr(n_threads)
        
	nexec+=1
	k=0
        while(1) do begin
		for j=0L,n_threads-1 do status[j]=(*bridges[j])->status()
		;print,status
		if((where(status ge 3))[0] ne -1) then print,"error"
		if (total(status) eq 0) then break
		if(k gt 2) then wait,wait_time	
		nsync+=1
		k+=1
        endwhile
        ;print," "
        nsyncperexec=float(nsync)/float(nexec)
        if(irep gt 1) then begin 
                if(nsyncperexec lt 6 and wait_time ge 1E-3) then begin
                        wait_time/=2.D
                        print,'New sync time =  '+string(strtrim(round(wait_time*1E3),1))+" ms"
    	        endif else if(nsyncperexec gt 50 and wait_time le 1) then begin
    	                wait_time*=2.D
                        print,'New sync time =  '+string(strtrim(round(wait_time*1E3),1))+" ms"
                endif
        endif
        for i=0L,n_threads-1 do begin
                fitns[st[i]:nd[i]]=(*bridges[i])->getvar("fitns")
        endfor
        return,fitns
        
        nothreading:
        if(size(xtra,/type) eq 8) then begin
        	fitns = call_function(funcname,n,oldph,parinfo=pinfo,_extra=xtra)
        endif else begin
        	fitns = call_function(funcname,n,oldph,parinfo=pinfo)
        endelse

        return,fitns
        
end

function ran2
        ; Common block to make iseed visible to urand_init (and to save
        ; it between calls)
        common share1
        common share2
        im1=2147483563 
        im2=2147483399 
        am=1./im1 
        imm1=im1-1
        ia1=40014 
        ia2=40692 
        iq1=53668 
        iq2=52774 
        ir1=12211 
        ir2=3791
        ntab=32 
        ndiv=1+imm1/ntab 
        eps=1.2E-7 
        rnmx=1.-eps
        idum = iseed
        if (idum le 0) then begin
                idum= -idum > 1
                idum2=idum
                for j=NTAB+8,1,-1 do begin
                        k=idum/IQ1
                        idum=(IA1*(idum-k*IQ1)-k*IR1)
                        if (idum lt 0) then idum=(idum+IM1)
                        if (j le NTAB) then iv(j-1)=idum
	        endfor
                iy=iv(0)
        endif
        k=idum/IQ1
        idum=IA1*(idum-k*IQ1)-k*IR1
        if (idum lt 0) then idum=idum+IM1 
        k=idum2/IQ2
        idum2=IA2*(idum2-k*IQ2)-k*IR2
        if (idum2 lt 0) then idum2=idum2+IM2 
        j=1+iy/NDIV
        iy=iv(j-1)-idum2
        iv(j-1)=idum
        if(iy lt 1) then iy=iy+IMM1 
        ran2= AM*iy < RNMX
        iseed = (idum)
        return,ran2
end

function urand
      common share1,iseed
      ; Return the next pseudo-random deviate from a sequence which is
      ; uniformly distributed in the interval [0,1]
      ;
      ; urand = randomu( iseed )
      urand = ran2()
      return,urand
end

pro indexx,n,arr,indx      
        M=11
        NSTACK=50
        istack = lonarr(NSTACK)
        arr1 = fltarr(n+1)
        for i = 0,n-1 do arr1(i+1) = arr(i)
        indx1 = indgen(n+1)
        indx = lonarr(n)
        jstack=0
        l=1
        ir=n
        label:
        if ir-l lt M then begin
	        for j = l+1,ir do begin
                        indxt=indx1(j)
                        a=arr1(indxt)
                        for i=j-1,l,-1 do begin
                                if arr1(indx1(i)) le a then goto,label2
                                indx1(i+1)=indx1(i)
	                endfor
                        i=l-1
	                label2:
                        indx1(i+1)=indxt
	        endfor
                if(jstack eq 0) then begin
	                for ll = 0,n-1 do indx(ll) = indx1(ll+1)
                        return
	        endif
                ir=istack(jstack)
                l=istack(jstack-1)
                jstack=jstack-2
        endif else begin
                k=(l+ir)/2
                itemp=indx1(k)
                indx1(k)=indx1(l+1)
                indx1(l+1)=itemp
                if(arr1(indx1(l+1)) gt arr1(indx1(ir))) then begin
                        itemp=indx1(l+1)
                        indx1(l+1)=indx1(ir)
                        indx1(ir)=itemp
                endif
                if(arr1(indx1(l)) gt arr1(indx1(ir))) then begin
                        itemp=indx1(l)
                        indx1(l)=indx1(ir)
                        indx1(ir)=itemp
                endif
                if(arr1(indx1(l+1)) gt arr1(indx1(l))) then begin
                        itemp=indx1(l+1)
                        indx1(l+1)=indx1(l)
                        indx1(l)=itemp
                endif
                i=l+1
                j=ir
                indxt=indx1(l)
                a=arr1(indxt)
                label3:
                i=i+1
                if(arr1(indx1(i)) lt a) then goto,label3
	        label4:
                j=j-1
                if(arr1(indx1(j)) gt a) then goto,label4
                if(j lt i) then goto,label5
                itemp=indx1(i)
                indx1(i)=indx1(j)
                indx1(j)=itemp
                goto,label3
	        label5:       
	        indx1(l)=indx1(j)
                indx1(j)=indxt
                jstack=jstack+2
                if(jstack gt NSTACK) then print,'NSTACK too small in indexx'
                if(ir-i+1 ge j-1) then begin
                        istack(jstack)=ir
                        istack(jstack-1)=i
                        ir=j-1
	        endif else begin
                        istack(jstack)=j-1
                        istack(jstack-1)=l
                        l=i
                endelse
        endelse
        goto,label
end

pro setctl,ctrl,n,np,ngen,nd,pcross,pmutmn,pmutmx,pmut,imut,fdif,irep,ielite,ivrb,status
        ; Set control variables and flags from input and defaults
        ; 
        ; Input: n
        ; Input/Output: ctrl(12)
        ; Output: np, ngen, nd, imut, irep, ielite, ivrb, status, pcross, pmutmn, pmutmx, pmut, fdif
        ; 
        DFAULT = [100,20,4,.85,2,.005,.0005,.25,1,1,1,1]
        test = where(ctrl lt 0)
        if min(test) gt -1 then ctrl(test) = DFAULT(test)
        np = long(ctrl(0))
        ngen = long(ctrl(1))
        nd = long(ctrl(2))
        pcross = ctrl(3)
        imut = long(ctrl(4))
        pmut = ctrl(5)
        pmutmn = ctrl(6)
        pmutmx = ctrl(7)
        fdif = ctrl(8)
        irep = long(ctrl(9))
        ielite = long(ctrl(10))
        ivrb = long(ctrl(11))
        status = 0
        ; Print a header
        if (ivrb gt 0) then begin
                f2 = '(/1x,60("*"),/'
                f2 = f2+'" *",13x,"PIKAIA Genetic Algorithm Report ",13x,"*",/'
	        f2 = f2 + '1x,60("*"),//,"   Number of Generations evolving: ",i4,/'
                f2 = f2 + '"       Individuals per generation: ",i4,/'
                f2 = f2 +'"    Number of Chromosome segments: ",i4,/'
	        f2 = f2 + '"    Length of Chromosome segments: ",i4,/'
	        f2 = f2 + '"            Crossover probability: ",f9.4,/'
	        f2 = f2 + '"            Initial mutation rate: ",f9.4,/'
	        f2 = f2 + '"            Minimum mutation rate: ",f9.4,/'
	        f2 = f2 + '"            Maximum mutation rate: ",f9.4,/'
	        f2 = f2 + '"    Relative fitness differential: ",f9.4)'
	        print,format=f2,ngen,np,n,nd,pcross,pmut,pmutmn,pmutmx,fdif
                f3='("                    Mutation Mode: ",A)'
                if (imut eq 1) then print,format=f3, 'Constant'
                if (imut eq 2) then print,format=f3, 'Variable'
                f4 = '("                Reproduction Plan: ",A)'
                if (irep eq 1) then print,format=f4, 'Full generational replacement'
                if (irep eq 2) then print,format=f4, 'Steady-state-replace-random'
                if (irep eq 3) then print,format=f4, 'Steady-state-replace-worst'
        endif
        ; Check some control values
        f10  = '" ERROR: illegal value for imut (ctrl(5))"'
        if (imut ne 1 and imut ne 2) then begin
                print,f10 
                status = 5
        endif
        f11 = '" ERROR: illegal value for fdif (ctrl(9))"'
        if (fdif gt 1) then begin
                print,f11
                status = 9
        endif 
        f12 = '" ERROR: illegal value for irep (ctrl(10))"'
        if (irep ne 1  and  irep ne 2  and  irep ne 3) then begin
	        print,f12
                status = 10
        endif
        f13 = ' ERROR: illegal value for pcross (ctrl(4))'
        if (pcross gt 1.0  or  pcross lt 0 ) then begin
	        print,f13
                status = 4
        endif
        f14 = ' ERROR: illegal value for pcross (ctrl(4))'
        if (ielite ne 0  and  ielite ne 1) then begin
 	        print,f14
                status = 11
        endif
        f15 = '" WARNING: dangerously high value for pmut (ctrl(6));",/" (Should enforce elitism with ctrl(11)=1.)"'
        if (irep eq 1  and  imut eq 1  and  pmut gt 0.5  and ielite eq 0) then begin
	        print,f15
        endif
        f16 = '" WARNING: dangerously high value for pmutmx (ctrl(8));",/" (Should enforce elitism with ctrl(11)=1.)"'
        if (irep eq 1 and imut eq 2 and pmutmx gt 0.5 and ielite eq 0) then begin
	        print,f16
        endif
        f17 = ' WARNING: dangerously low value of fdif (ctrl(9))'
        if (fdif lt 0.33) then begin
	        print,f17
        endif 
        return
end

pro report,ivrb,ndim,n,np,nd,oldph,fitns,ifit,pmut,ig,nnew,parinfo
        common share3
        common share4
        common datstore,bestft,pmutpv
        ; Write generation report to standard output
        ; 
        ; Input: ifit(np),ivrb,ndim,n,np,nd,ig,nnew,oldph(ndim,np),fitns(np),pmut
        ; 
        rpt=1.
        if (pmut ne pmutpv) then begin
                pmutpv=pmut
                rpt=1.
        endif
        if (fitns[ifit[np-1]] ne bestft) then begin
                bestft=fitns[ifit[np-1]]
                rpt=1.
        endif
        
        if(ig eq 0)then print,"   #Gen   #New pop   #Mutation Rate   #Best fitness   #Elasped seconds"
        
        if(ivrb eq 3) then begin
        	if(ig eq 0) then begin
                	window,1,xs=1024,ys=512,title="Parameters Distribution"
        	endif
        	if(n mod 2 eq 0) then nplot=n else nplot=n+1
        	!P.MULTI=[0,2,nplot/2,0,0]
        	wset,1
        	not_fixed = where(parinfo.FIXED eq 0,ct)

        	for i=0L,n-1 do begin
         	       if(parinfo[not_fixed[i]].parname ne "") then name=parinfo[not_fixed[i]].parname else name="Parameter "+strtrim(string(i),1)
                	domain=(parinfo[not_fixed[i]].limits[1]-parinfo[not_fixed[i]].limits[0])
                	offset=parinfo[not_fixed[i]].limits[0]
                	plothist,oldph[i,0:np-1]*domain+offset,bin=domain*0.01,xstyle=1,ystyle=1,$
                		xrange=[parinfo[not_fixed[i]].limits[0],parinfo[not_fixed[i]].limits[1]],title=name
        	endfor
        	!P.MULTI=0
        endif
        
        if (rpt ne 0.  or  ivrb ge 2) then begin
                ; Power of 10 to make integer genotypes for display
                ndpwr = round(10.^nd)
                print, ig+1,nnew+1,pmut,fitns[ifit[np-1]], round(systime(1)-start_time)
	        ;for k=0,n-1 do begin
                ;        print,format='(22x,3i10)',round(ndpwr*oldph[k,ifit[np-1]]),round(ndpwr*oldph[k,ifit[np-2]]),round(ndpwr*oldph[k,ifit[(np+1)/2 -1]])
	        ;endfor
        endif
                        
end

; ////////////////////////////////////////////////////////
; GENETICS MODULE
; \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\; 
; encode:    encodes phenotype into genotype
;            called by: PIKAIA
; 
; decode:    decodes genotype into phenotype
;            called by: PIKAIA
; 
; cross:     Breeds two offspring from two parents
;            called by: PIKAIA
; 
; mutate:    Introduces random mutation in a genotype
;            called by: PIKAIA
; 
; adjmut:    Implements variable mutation rate
;            called by: PIKAIA
; 

pro encode,n,nd,ph,gn
        ; Encode phenotype parameters into integer genotype
        ; ph(k) are x,y coordinates [ 0 < x,y < 1 ]
        ; 
        ; Inputs: n, nd, ph(n)
        ; Output: gn(n*nd)
        ;
        z=10.^nd
        ii=0
        for i=0,n-1 do begin
                ip = long(ph(i)*z)
                for  j=nd-1,0,-1 do begin
                        gn(ii+j)=ip mod 10
                        ip=ip/10
	        endfor
                ii=ii+nd
        endfor	 
        return
end
 
pro decode,n,nd,gn,ph
        ; Decode genotype into phenotype parameters
        ; ph(k) are x,y coordinates [ 0 < x,y < 1 ]
        ;  
        ; Inputs: n, nd, gn(n*nd)
        ; Output: ph(n)
        ; 
        z=10.^(-nd)
        ii=0
        for i=0,n-1 do begin
                ip=0
                for j=0,nd-1 do begin	
                        ip=10*ip+gn(ii+j)
	        endfor
                ph(i)=ip*z
                ii=ii+nd
        endfor
        return
end
 
pro cross,n,nd,pcross,gn1,gn2
        ; Breeds two parent chromosomes into two offspring chromosomes
        ; Breeding occurs through crossover starting at position ispl
        ; 
        ; Inputs: n, nd, pcross
        ; Input/Output: gn1(n*nd), gn2(n*nd)
        ;  
        ; Use crossover probability to decide whether a crossover occurs
        if (urand() lt pcross) then begin
                ; Compute crossover point
                ispl=long(urand()*n*nd)+1
                ; Swap genes at ispl and above
                for i=ispl-1,n*nd-1 do begin
                        t=gn2(i)
                        gn2(i)=gn1(i)
                        gn1(i)=t
	        endfor
        endif
        return
end
 
pro mutate,n,nd,pmut,gn
        ; Mutations occur at rate pmut at all gene loci
        ; 
        ; Input: n, nd, pmut
        ; Input/Output: gn(n*nd)
        ;
        ; Subject each locus to mutation at the rate pmut
        for i=0,n*nd-1 do begin
                if urand() lt pmut then begin
                        gn(i)=long(urand()*10.)
                endif
        endfor
        return
end
 
pro adjmut,np,fitns,ifit,pmutmn,pmutmx,pmut
        ; Dynamical adjustment of mutation rate; criterion is relative
        ; difference in absolute fitnesses of best and median individuals
        ; 
        ; Input: np, ifit(np), fitns(np), pmutmn, pmutmx
        ; Input/Output: pmut
        ;  
        rdiflo=0.05
        rdifhi=0.25  
        delta=1.5 
        rdif=abs(fitns(ifit(np-1))-fitns(ifit(np/2-1)))/(fitns(ifit(np-1))+fitns(ifit(np/2-1)))
        if rdif le rdiflo then pmut=pmutmx<pmut*delta
        if rdif ge rdifhi then pmut=pmutmn>pmut/delta
        return
end

; ////////////////////////////////////////////////////////
; REPRODUCTION MODULE
; \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
; select:   Parent selection by roulette wheel algorithm
;           called by: PIKAIA
; 
; rnkpop:   Ranks initial population
;           called by: PIKAIA, NEWPOP
; 
; genrep:   Inserts offspring into population, for full
;           generational replacement
;           called by: PIKAIA
; 
; stdrep:   Inserts offspring into population, for steady-state
;           reproduction
;           called by: PIKAIA
;           calls:     FNAME
; 
; newpop:   Replaces old generation with new generation
;           called by: PIKAIA
;           calls:     FNAME, RNKPOP
; 

pro select,np,jfit,fdif,idad
        ; Selects a parent from the population, using roulette wheel
        ; algorithm with the relative fitnesses of the phenotypes as
        ; the "hit" probabilities [see Davis 1991, chap. 1].
        ; 
        ; Input: np, jfit(np), fdif
        ; Output: idad
        ; 
        np1 = np+1
        dice = urand()*np*np1
        rtfit = 0.
        for i=0,np-1 do begin
                rtfit = rtfit+np1+fdif*(np1-2*(jfit(i)+1))
                if (rtfit ge dice) then begin
                        idad=i
                        goto,label22
                endif
        endfor
        ; Assert: loop will never exit by falling through
        label22: 
        return
end
 
pro rnkpop,n,arrin,indx,rank
        ; Calls external sort routine to produce key index and rank order
        ; of input array arrin (which is not altered).
        ;
        ; Input: n, arrin(n)
        ; Output: indx(n), rank(n)
        ;
        indx = lonarr(n)
        rank = lonarr(n)
        ; Numerical Recipes subroutine
        ; external indexx
        ;
        ; Compute the key index
        indexx,n,arrin,indx
        indx = indx-1
        ; ...and the rank order
        for  i=0,n-1 do begin
                rank(indx(i)) = n-i-1
        endfor
        return
end
 
pro genrep,ndim,n,np,ip,ph,newph
        ; Full generational replacement: accumulate offspring into new
        ; population array
        ; 
        ; Input: ndim, n, np, ip, ph(ndim,2)
        ; Output: newph(ndim,np)
        ; 
        ; Insert one offspring pair into new population
        i1=2*ip
        i2=i1+1
        
        noff = (size(ph))[2]
        i=indgen(noff)
        i+=noff*ip
        newph[0:n-1,i]=ph[0:n-1,0:noff-1]
        return
end
 
pro stdrep,ndim,n,np,irep,ielite,ph,oldph,fitns,ifit,jfit,nnew,_extra=extra
        common share3
        common share4
        ; Steady-state reproduction: insert offspring pair into population
        ; only if they are fit enough (replace-random if irep=1 or
        ; replace-worst if irep=2).
        ; 
        ; Input: ndim, n, np, irep, ielite, ph(ndim,2)
        ; Input/Output: oldph(ndim,np), fitns(np), ifit(np), jfit(np)
        ; Output: nnew
        ;
        nnew = 0
        noff = (size(ph))[2]
        ; 1. Compute offspring fitness (with caller's fitness function)
        fit=bridge_function_wrapper(fname,n,ph[0:n-1,0:noff-1],_extra=extra)
        for j=0,noff-1 do begin
        ; 2. if fit enough, insert in population
        for i=np-1,0,-1 do begin
                if (fit[j] gt fitns[ifit[i]]) then begin
                        ; Make sure the phenotype is not already in the population
                        if (i lt np-1) then begin
                                for  k=0,n-1 do begin
                                        if (oldph[k,ifit[i+1]] ne ph[k,j]) then goto,label6
		                endfor
                                goto,label11
		                label6:
                        endif
                        ; Offspring is fit enough for insertion, and is unique
                        ; (i) insert phenotype at appropriate place in population
                        if (irep eq 3) then i1=0 else begin
		                if (ielite eq 0 or i eq np-1) then begin
			                i1=long(urand()*np)
		                endif else begin
			                i1=long(urand()*(np-1))
		                endelse
	                endelse
                        if1 = ifit(i1)
                        fitns[if1]=fit[j]
	                oldph[*,if1] = ph[*,j]
                        ; (ii) shift and update ranking arrays
                        if (i lt i1) then begin
                                ; Shift up
                                jfit[if1]=np-i-2
                                for k=i1-1,i+1,-1 do begin
                                        jfit[ifit[k]]=jfit[ifit[k]]-1
                                        ifit[k+1]=ifit[k]
		                endfor
                                ifit[i+1]=if1
	                endif else begin
                                ; Shift down
                                jfit[if1]=np-i-1
                                for k=i1+1,i do begin
                                        jfit[ifit[k]]=jfit[ifit[k]]+1
                                        ifit[k-1]=ifit[k]
	                        endfor
                                ifit[i]=if1
                        endelse
                        nnew = nnew+1
                        goto,label11
                endif
	endfor
        label11:
        endfor
        return
end

 
pro newpop,ielite,ndim,n,np,oldph,newph,ifit,jfit,fitns,nnew,_extra=extra
        common share3
        ; Replaces old population by new; recomputes fitnesses & ranks
        ;
        ; Input: ndim, np, n, ielite
        ; Input/Output: oldph(ndim,np), newph(ndim,np)
        ; Output: ifit(np), jfit(np), nnew, fitns(np)
        nnew = np
        ; If using elitism, introduce in new population fittest of old
        ; population (if greater than fitness of the individual it is
        ; to replace)
        ;
        fitns_elite=call_function(fname,n,newph[0:n-1,0],parinfo=pinfo,_extra=extra)
        if (ielite eq 1 and fitns_elite[0] lt fitns(ifit[np-1])) then begin
                for k=0,n-1 do begin
                        newph[k,0]=oldph[k,ifit[np-1]]
	        endfor
                nnew = nnew-1
        endif
        ; Replace population
        ; and get fitness using caller's fitness function
        ; but be careful of zeros
        for i = 0,np-1 do begin
                if newph[0,i] ne 0. then begin
	                oldph[*,i] = newph[*,i]
	        endif
        endfor
        fitns[0:np-1] = bridge_function_wrapper(fname,n,oldph[0:n-1,0:np-1],_extra=extra)
        ; Compute new population fitness rank order
        rnkpop,np,fitns,ifit,jfit
        return
end
      
pro pikaia,n,ctrl,x,f,status,oldph=oldph,fitns=fitns,fname=functionname,seed=seed,threading=thrd,parinfo=parinfo,_extra=extra	
	def_common_blocks
	common share1
	common share2
	common share3
	common share4
	common synchro
	common datstore
	start_time=systime(1)
        fname=functionname
        if(keyword_set(extra)) then xtra=extra else xtra=0
        pinfo=parinfo
        if(keyword_set(thrd)) then threading=thrd else threading=0
        
        wh = where(parinfo.FIXED eq 0,n)

        ;
        ; Optimization (maximization) of user-supplied "fitness" function
        ; fname  over n-dimensional parameter space  x  using a basic genetic
        ; algorithm method.
        ;
        ; (IDL version 1996 July - comments kept intact)
        ;
        ;  Genetic algorithms are heuristic search techniques that
        ;  incorporate in a computational setting, the biological notion
        ;  of evolution by means of natural selection.  This subroutine
        ;  implements the three basic operations of selection, crossover,
        ;  and mutation, operating on "genotypes" encoded as strings.
        ;  
        ;  References:
        ;  
        ;     Charbonneau, Paul.  "Genetic Algorithms in Astronomy and
        ;        Astrophysics."  Astrophysical J. (Supplement), vol 101,
        ;        in press (December 1995).
        ;  
        ;     Goldberg, David E.  Genetic Algorithms in Search, Optimization,
        ;        & Machine Learning.  Addison-Wesley, 1989.
        ;  
        ;     Davis, Lawrence, ed.  Handbook of Genetic Algorithms.
        ;        Van Nostrand Reinhold, 1991.
        ;  
        ;  o Integer  n  is the parameter space dimension, i.e., the number
        ;    of adjustable parameters.  (Also the number of chromosomes.)
        ; 
        ;  o Function  fname  is a user-supplied scalar function of n vari-
        ;    ables, which must have the calling sequence f = ff(n,x), where
        ;    x is a real parameter array of length n.  This function must
        ;    be written so as to bound all parameters to the interval [0,1];
        ;    that is, the user must determine a priori bounds for the para-
        ;    meter space, and ff must use these bounds to perform the appro-
        ;    priate scalings to recover true parameter values in the
        ;    a priori ranges.
        ; 
        ;    By convention, ff should return higher values for more optimal
        ;    parameter values (i.e., individuals which are more "fit").
        ;    For example, in fitting a function through data points, ff
        ;    could return the inverse of chi**2.
        ; 
        ;    In most cases initialization code will have to be written
        ;    (either in a driver or in a separate subroutine) which loads
        ;    in data values and communicates with ff via one or more labeled
        ;    common blocks.  An example exercise driver and fitness function
        ;    are provided in the accompanying file, xpikaia.f.
        ; 
        ; 
        ;  Input/Output: ctrl(12)
        ; o Array  ctrl  is an array of control flags and parameters, to
        ;   control the genetic behavior of the algorithm, and also printed
        ;   output.  A default value will be used for any control variable
        ;   which is supplied with a value less than zero.  On exit, ctrl
        ;   contains the actual values used as control variables.  The
        ;   elements of ctrl and their defaults are:
        ; 
        ;      ctrl( 1) - number of individuals in a population (defaultabst
        ;                 is 100)
        ;      ctrl( 2) - number of generations over which solution is
        ;                 to evolve (default is 500)
        ;      ctrl( 3) - number of significant digits (i.e., number of
        ;                 genes) retained in chromosomal encoding (default
        ;                 is 6)  (Note: This number is limited by the
        ;                 machine floating point precision.  Most 32-bit
        ;                 floating point representations have only 6 full
        ;                 digits of precision.  To achieve greater preci-
        ;                 sion this routine could be converted to double
        ;                 precision, but note that this would also require
        ;                 a double precision random number generator, which
        ;                 likely would not have more than 9 digits of
        ;                 precision if it used 4-byte integers internally.)
        ;      ctrl( 4) - crossover probability; must be  <= 1.0 (default
        ;                 is 0.85)
        ;      ctrl( 5) - mutation mode; 1/2=steady/variable (default is 2)
        ;      ctrl( 6) - initial mutation rate; should be small (default
        ;                 is 0.005) (Note: the mutation rate is the proba-
        ;                 bility that any one gene locus will mutate in
        ;                 any one generation.)
        ;      ctrl( 7) - minimum mutation rate; must be >= 0.0 (default
        ;                 is 0.0005)
        ;      ctrl( 8) - maximum mutation rate; must be <= 1.0 (default
        ;                 is 0.25)
        ;      ctrl( 9) - relative fitness differential; range from 0
        ;                 (none) to 1 (maximum).  (default is 1.)
        ;      ctrl(10) - reproduction plan; 1/2/3=Full generational
        ;                 replacement/Steady-state-replace-random/Steady-
        ;                 state-replace-worst (default is 3)
        ;      ctrl(11) - elitism flag; 0/1=off/on (default is 0)
        ;                 (Applies only to reproduction plans 1 and 2)
        ;      ctrl(12) - printed output 0/1/2/3=None/Minimal/Verbose/Histogram
        ;                 (default is 0)
        ; Output:
      	x = fltarr(n)
        ;     integer   status
        ;
        ; o Array  x(1:n)  is the "fittest" (optimal) solution found,
        ;       i.e., the solution which maximizes fitness function ff
        ;
        ; o Scalar  f  is the value of the fitness function at x
        ;
        ; o Integer  status  is an indicator of the success or failure
        ;         of the call to pikaia (0=success; non-zero=failure)
        NMAX = 32
        PMAX = 1024 
        DMAX = 8
        ;
        ; o NMAX is the maximum number of adjustable parameters
        ;       (n <= NMAX)
        ;
        ; o PMAX is the maximum population (ctrl(1) <= PMAX)
        ;
        ; o DMAX is the maximum number of Genes (digits) per Chromosome
        ;       segement (parameter) (ctrl(3) <= DMAX)
        ;
        ; Some variables definitions for the ran2 random number generator
        NTAB=32 
        idum2=123456789
        iv=lonarr(NTAB) 
        iv[*]= 0
        iy=0
        ;Launching all threads if the option is specified
        noff=2
        if(threading) then begin
        	init_threads
        	;?????noff=n_threads
	endif
        
      	ph = dblarr(nmax,noff)
      	oldph = dblarr(nmax,pmax)
      	newph = dblarr(nmax,pmax)
      	gn1 = lonarr(nmax*dmax)
      	gn2 = lonarr(nmax*dmax)
      	ifit = lonarr(pmax)
      	jfit = lonarr(pmax)
      	fitns = dblarr(pmax)
        pmutpv = 0.D
        bestft = 0.D
        countlastpop=intarr(3)
        ; Check the if the user provide a seed for the random number generator 
        if(~keyword_set(seed)) then seed=systime(1)
        seed = -abs(long(seed))
        iseed=seed
        ; Set control variables from input and defaults
        setctl,ctrl,n,np,ngen,nd,pcross,pmutmn,pmutmx,pmut,imut, fdif,irep,ielite,ivrb,status
        if (status  ne  0) then begin
                print," Control vector (ctrl) argument(s) invalid"
                return
        endif
        ; Make sure locally-dimensioned arrays are big enough
        if (n gt NMAX  or  np gt PMAX  or  nd gt DMAX) then begin
                print," Number of parameters, population, or genes too large"
                status = -1
                return
        endif
        ; Compute initial (random but bounded) phenotypes
        for ip=0,np-1 do begin
                for k=0,n-1 do begin
                        oldph[k,ip]=urand()
      	        endfor
        endfor
        ; We measure the number of call with the following variable
        ; This is usefull for adjusting automatically the synchronistion time when the threading keyword is set
        nexec=0
        nsync=0
        ; Evaluate the fitness of the first generation
        fitns[0:np-1] = bridge_function_wrapper(fname,n,oldph[0:n-1,0:np-1],_extra=extra)
        ; Rank initial population by fitness order
        rnkpop,np,fitns,ifit,jfit
        ; Main Generation Loop
        for ig=0,ngen-1 do begin
                ; Main Population Loop
                newtot=0
                ctr=0
                for ip=0,np/noff-1 do begin
                        ; Loop until noff pairs are produced
                        for ioff=0,noff-1,2 do begin
                                ;1. Pick two parents
                                select,np,jfit,fdif,ip1
                                label21:
                                select,np,jfit,fdif,ip2
                                if (ip1 eq ip2) then goto,label21
                                ;2. Encode parent phenotypes
                                encode,n,nd,oldph[0:n-1,ip1],gn1
                                encode,n,nd,oldph[0:n-1,ip2],gn2
                                ;3. Breed
                                cross,n,nd,pcross,gn1,gn2
                                mutate,n,nd,pmut,gn1
                                mutate,n,nd,pmut,gn2
                                ;4. Decode offspring genotypes
      	                        phsub = fltarr(n)
      	                        phsub = ph[0:n-1,0+ioff]
                                decode,n,nd,gn1,phsub
      	                        ph[0:n-1,0+ioff] = phsub[*]
      	                        phsub = ph[0:n-1,1+ioff]
                                decode,n,nd,gn2,phsub
                                ph[0:n-1,1+ioff] = phsub[*]
                        endfor
                        ;5. Insert into population
                        if (irep eq 1) then begin
                                genrep,NMAX,n,np,ip,ph,newph
      	                endif else begin
                                stdrep,NMAX,n,np,irep,ielite,ph,oldph,fitns,ifit,jfit,new,_extra=extra
                                newtot = newtot+new
                        endelse
                        ; End of Main Population Loop
       	        endfor
                ; If running full generational replacement: swap populations
                if (irep eq 1) then newpop,ielite,NMAX,n,np,oldph,newph,ifit,jfit,fitns,newtot,_extra=extra
                ; Adjust mutation rate?
                if (imut eq 2) then adjmut,np,fitns,ifit,pmutmn,pmutmx,pmut
                ; Print generation report to standard output?
                if (ivrb gt 0) then report,ivrb,NMAX,n,np,nd,oldph,fitns,ifit,pmut,ig,newtot,parinfo
                ; End of Main Generation Loop
                if (ig lt 3) then begin
                	countlastpop[ig] = newtot 
                endif else begin
                	countlastpop	= shift(countlastpop,-1)
                	countlastpop[2]	= newtot
		endelse
		; If the  the mean of new individuals introduced in the main pop
		; over the 3 last generations is lt 1.5, and it's been more than 10 generation
		; then there is no more evolution: the fit stops
		if(mean(countlastpop) lt 1.5 and ig gt 10) then begin
			print,"No more evolution"
			break
		endif
        endfor
        ; Return best phenotype and its fitness 
        x[*] = oldph[0:n-1,ifit[np-1]]
        std_dev = x
        for i=0L,n-1 do std_dev[i]=stddev(oldph[i,0:np-1])
        f = fitns[ifit[np-1]]
        if(threading) then begin
        	print,"Killing threads"
        	kill_threads
        endif
        
end