;+
; This routine convert a image of a size n*n to the tab format used in kinemetry routine
;
; NAME:
;	IMTOTABS
;
; INPUTS:
;	IM:
;		A 2D array containing the reconstructed image from the 3 inputs
;
; OUTPUTS:
;	XBIN:
;		An array containing the x postions of each bin
;	YBIN:
;		An array containing the y position of each bin
;	ZBIN:
;		An array containing the measurement associated to each bin
;
; OPTIONAL OUTPUTS:
;	XCENTER:
;		The position of the X component of the center (middle of the map)
;	YCENTER:
;		The position of the Y component of the center (middle of the map)
;
;-
pro imtotabs,im,xbin,ybin,zbin,xcenter=xcenter,ycenter=ycenter
	
	if(n_params() ne 4) then begin
		doc_library,'imtotabs'
		return
	endif
	sizex=(size(im))[1]
        sizey=(size(im))[2]

        xcenter=(sizex-1)/2.D
        ycenter=(sizey-1)/2.D

        cpt=0L
	
	xbin=fltarr(n_elements(where(finite(im))))
        ybin=fltarr(n_elements(where(finite(im))))
        zbin=fltarr(n_elements(where(finite(im))))
	
	
	for i=0L,sizex-1L do begin
            for j=0L,sizey-1L do begin
                if(finite(im[i,j])) then begin
                    xbin[cpt]=(i-xcenter)
                    ybin[cpt]=(j-ycenter)
                    zbin[cpt]=im[i,j]
                    cpt+=1L
                endif
            endfor
        endfor
   
end