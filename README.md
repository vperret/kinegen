## KINEGEN ##

KINEGEN is a set of IDL routines that fit a galaxy velocity field using a genetic algorithm combined with an harmonic decomposition of the velocity field method called kinemetry.
The user can feed the routine with a FITS format velocity field, and the routine will output the best fit center coordinates, the position axis, and the inclination of the galactic disk.
More details are contained in the main routine header.
The routine is able to run on several CPUs using IDL bridge.